---
layout: default
#title: CoCoDev - no title otherwise double menu item
---


<div>
          <img alt="CoCodev logo" src="{{ site.baseurl }}/img/logo.jpeg" style="width: 100%;"/>
        </div>
    



<!--<div class="home">-->




<p><br> </p>
<!--
<div class='jekyll-twitter-plugin' align="center">
    {% twitter https://twitter.com/Adinseg maxwidth=300 limit=5 %}
</div>
-->

<div class="row mb-4 row-flex">

    <div class="col-md-8 mb-2" id="news-col">
      <!-- <div class="card h-100"> -->
	<div class="card" style="height:600px;">
        <div class="card-title container-fluid m-0">
          <h5 class="font-weight-bold mt-2 ml-2 mr-2 mb-2">News</h5>
        </div>
    <div class="card-body container-fluid overflow-auto mt-0">
  {% assign currentsorted = site.data.news %}            
  {% for news in currentsorted %}
    {% include news.md %}
  {% endfor %}
 	</div>
        
      </div>
    </div>

<div class="col-md-4 mb-2">
  <div class="card" style="height:600px;">
    <div class="card-body">
      <link rel="stylesheet" href="https://embedbsky.com/embedbsky.com-master-min.css" /><div id="embedbsky-com-timeline-embed"></div><script>let containerWidth='200%',containerHeight=550;const getHtml=async t=>{const e=await fetch(t);return 200!==e.status?'<p><strong>No feed data could be located</p></strong>':e.text()};document.addEventListener('DOMContentLoaded',(async()=>{const t=(new Date).toISOString(),e=document.getElementById('embedbsky-com-timeline-embed');e.style.width="100%",e.style.height=`${containerHeight}px`;const n=await getHtml(`https://embedbsky.com/feeds/1639785d5879331a15665bbe078a3c16f9814a353bf9e40b3869846443a91513.html?v=${t}`);e.innerHTML=n}));</script>
    </div>
  </div>
</div>

</div>



<!--<p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>-->

<!--</div>-->
