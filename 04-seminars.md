---
layout: page
title: Seminars
permalink: /seminars/
---

<p> <br></p>
You can attend an upcoming online seminar or see the details of previous ones (including video recordings when available). The details are given in the description box.  In addition, you can download the seminars' agenda to be updated in real time: [CoCoDev's Google agenda](https://calendar.google.com/calendar/u/0?cid=MnRwN280ZXQzbXJtb2F0NTEzaHAxdHQ4YW9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ).

<p> <br></p>
<!-- To edit the list of seminars, you must modify _data/seminars.yml -->

{% assign currentdate = "now" | date: "%Y-%m-%d" %}
{% assign sortedseminars = site.data.seminars | sort: "date" %}

### Upcoming

{% assign pastorfuture = "future" %}

<div class = "container-fluid">
  <div class = "accordion">  
  {% for seminar in sortedseminars %}
    {% assign seminardate = seminar.date | date: "%Y-%m-%d" %}
    {% if seminardate >= currentdate %}
      {% include seminar.md %}     
    {% endif %}
  {% endfor %}
  </div>
</div>

<hr class="my-5">
<!--------------------------------------------------------------------->

### Past

{% assign pastorfuture = "past" %}

<div class = "container-fluid">
  <div class = "accordion">  
  {% for seminar in sortedseminars reversed %}
    {% assign seminardate = seminar.date | date: "%Y-%m-%d" %}
    {% if seminardate < currentdate %}
      {% include seminar.md %}     
    {% endif %}
  {% endfor %}
  </div>
</div>



