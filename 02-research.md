---
layout: page
title: Research
permalink: /research/
---
<div class="center-div">

<!--
<center><h2 style="color:#0092ca;">In progress</h2> </center>
{% bibliography --query @*[year=under review] %}
-->
<br><br>
Our research program focuses on scaling the study of children’s language development to reflect the complexity of real-world learning environments, leveraging Big Data and cutting-edge Machine Learning. 
<br><br>
This approach is essential to improve the ecological validity of our scientific theories and to facilitate their translation into practical tools that support better language learning outcomes.

<br><br>
The CoCoDev team investigates the key elements that shape how children learn language: sensory input, cognitive mechanisms, and social interactions.
<br><br>



<div>
          <img alt="research" src="https://cocodev1.gitlab.io/website/img/research.png">
        </div> 


<br><br>
<center><h2>Sensory input</h2> </center>
<br><br>

Language development starts with the sensory experiences of children—the words they hear and the things they see. We collect and analyze data from these experiences to understand how their richness influences learning.
<br><br>
One line of inquiry is exploring how children learn linguistic structures like sounds and grammar from natural speech. We specifically focus on how they manage to overcome the variability and noise to form stable mental representations: 

<br><br>
{% bibliography --query @*[section=input1] %}

Additionally, we examine how sensory input helps children map language onto conventional meanings. This process is complex, involving not only the recognition of visual referents like "apple" but also the inference of abstract meanings for words such as "animal," "yesterday," or 'twenty":

<br><br>

{% bibliography --query @*[section=input2] %}
<br><br>


<center><h2>Cognitive mechanisms</h2> </center>
<br><br>
While rich sensory input is crucial, children's cognitive abilities determine how effectively this input is transformed into linguistic knowledge.
<br><br>

We study the cognitive mechanisms children use to process sensory input, such as their ability to combine information from different modalities (e.g., mapping sounds to meaning). Using cognitive modeling, we mathematically characterize these skills and make quantitative predictions about learning. These predictions are tested both in the lab and in natural settings:
<br><br>
{% bibliography --query @*[section=cognitive1] %}

We also investigate how language is refined over months and years of development. Our methods allow us to study the complex interactions between how knowledge is organized in a child’s long-term memory (e.g., the lexical network) and the characteristics of their natural learning environment:

<br><br>
{% bibliography --query @*[section=cognitive2] %}
<br><br>

<center><h2>Social interactions</h2> </center>

<br><br>
The transformation of sensory input into linguistic knowledge is facilitated by social interactions. We study early interactions between children and caregivers to understand how and when communicative skills emerge and how these skills specifically support language acquisition.

<br><br>

Automated tools enable us to analyze these interactions on a large scale as they naturally occur in children's environments. This approach offers valuable insights into how children communicate spontaneously, learning to take turns and coordinate shared meanings:

<br><br>
{% bibliography --query @*[section=social1] %}


We investigate how these communication skills directly influence the effectiveness of language learning, with a particular focus on the critical role of social feedback in the learning process:


<br><br>
{% bibliography --query @*[section=social2] %}

<br><br>
In conclusion, we leverage modern Machine Learning to develop theories of language development that can be tested and refined in real-world settings. This ecological approach ensures that our findings are both scientifically robust and more directly applicable. For instance, it can guide the design of educational tools and therapeutic interventions grounded in theory, which, by confronting other real-world challenges, provide critical insights for further refining those theories.

</div>
