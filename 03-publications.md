---
layout: page
title: Publications
permalink: /bibilography/
---
<p><br></p>

<!-- <center><h3>Under review</h3> </center>
{% bibliography --query @*[year=under review] %}
-->

<center><h3>2025</h3> </center>
{% bibliography --query @*[year=2025] %}

<center><h3>2024</h3> </center>
{% bibliography --query @*[year=2024] %}

<center><h3>2023</h3> </center>
{% bibliography --query @*[year=2023] %}

<center><h3>2022</h3> </center>
{% bibliography --query @*[year=2022] %}

<center><h3>2021</h3> </center>
{% bibliography --query @*[year=2021] %}

<center><h3>2020</h3> </center>
{% bibliography --query @*[year=2020] %}

<center><h3>2029 and older </h3> </center>
{% bibliography --query @*[year<2020] %}

<p><br></p>
