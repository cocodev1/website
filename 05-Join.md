---
layout: page
title: Join us
permalink: /contact/
---

<p><br></p>

## Now hiring! (update: Position filled)

{% for post in site.posts %}
<a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
{% endfor %}


<p><br></p>
<p><br></p>
## Other opportunities to join
<p><br></p>
### Ph.D. researchers and Postdocs

We regularly publish announcements for available Ph.D. and Postdoc positions (you can follow the news section for such announcements).  In addition, our members would be delighted to help you put together an application for a fellowship to come work within the team. 

For prospective Ph.D. researchers, there are several options such as:

- The ministerial PhD scholarships (especially [ED356](https://ecole-doctorale-356.univ-amu.fr/) and [ED186](https://ecole-doctorale-184.univ-amu.fr/)) 

- [ILCB Institute PhD scholarships](https://www.ilcb.fr/2-phd-grants-on-any-topic-that-falls-within-the-area-of-language-communication-brain-and-modelling/)

- [Archimedes Institute PhD scholarships](https://labex-archimede.univ-amu.fr/en/programs/phd-positions-2021-call.html)

- [Partenariats Hubert Curien (PHC) scholarships](https://www.campusfrance.org/fr/phc)

For prospective Postdocs, there are several funding mechanisms such as:

- [The ILCB fellowship](https://www.ilcb.fr/ilcb-fellow/)

- [The Fyssen Foundation postdoctoral study grant](http://www.fondationfyssen.fr/en/study-grants/aim-award/)

- [The Marie Skłodowska-Curie Action (MSCA) postdoctoral fellowship](https://ec.europa.eu/research/mariecurieactions/actions/postdoctoral-fellowships)


Please reach out to us to discuss what would be best for you.

<p><br></p>

### Master Students 

Each year, our team hosts several students to prepare their master’s thesis in M1 or M2.  Since our team is highly interdisciplinary, students from several masters (e.g., in Cognitive science, Neuroscience, Artificial intelligence) or engineering schools are welcome. If you are interested in the research we do in our team, please get in touch with us as early as possible in the academic year to plan the research project ahead (especially if you are coming from outside Marseille or from a different country). The master’s internships in France typically start in  February and the students are paid if they work full time (which is typically the case for M2).
<p><br></p>

### Summer interns 
We have a limited number of paid internships available every summer. Students from different backgrounds are welcome to apply informally by sending a CV and a transcript (relevé de notes).  Master-level, undergraduate (i.e., L2 or L3) as well as engineering students can apply. The specific details of the research project will be discussed with the student to fit both their skills and interests. The positions last from 2 to 3 months. They are open until filled, but early applications may enjoy an improved odd of being selected. 

