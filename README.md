![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

CoCoDev website sources using [Jekyll][] and [GitLab Pages][].  
--------------------------------------------------------------

View it live at https://cocodev1.gitlab.io/website/


