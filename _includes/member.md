  <div class="col-sm-3 my-1">
	<div class="card text-center" style="height:100%">  
	  {% if member.website %}
		<a href="{{ member.website }}">
	  {% else %}
		<span>
	  {% endif %}
	  {% if member.picture %}
		<img class="card-img-top pt-2 px-1" src="../{{ member.picture }}" alt="Photo of {{ member.name }}"/>
	  {% else %}
		<img class="card-img-top pt-2 px-1" src="../img/members/anonymous{% cycle "0", "1", "2", "3" %}.jpg" alt="Anonymous photo"/>        
	  {% endif %}
	  {% if member.website %}
		</a>
	  {% else %}
		</span>
	  {% endif %}
	  <div class = "card-body">
		<h6 class = "card-title">
		  {% if member.website %}
			<a href="{{ member.website }}">
		  {% endif %}
		  {{ member.name }}
		  {% if member.website %}
			</a>
		  {% endif %}
		</h6>
		<span class="text-muted small">
		{% if member.title %}
		  {{ member.title }}
		{% endif %}
		{% if member.end %}
		  ({{ member.start }}-{{ member.end }})
		{% endif %}
		</span>
	  </div>
	  <div class = "card-footer text-muted">                  
	  {% if member.email %}
		<a href = "mailto:{{ member.email }}"><img height="20px" src="../img/clipart/email.png"/></a>
	  {% endif %}        
	  {% if member.website %}
		<a href = "{{ member.website }}"><img height="20px" src="../img/clipart/website.png"/></a>
	  {% endif %}        
	  {% if member.twitter %}
		<a href = "{{ member.twitter }}"><img height="20px" src="../img/clipart/twitter.png"/></a>
	  {% endif %}        
	  {% if member.linkedin %}
		<a href = "{{ member.linkedin }}"><img height="20px" src="../img/clipart/linkedin.png"/></a>
	  {% endif %}        
	  {% if member.resgate %}
		<a href = "{{ member.resgate }}"><img height="20px" src="../img/clipart/researchgate.png"/></a>
	  {% endif %}        
	  {% if member.acl %}
		<a href = "{{ member.acl }}"><img height="20px" src="../img/clipart/acl.png"/></a>
	  {% endif %}
	  {% unless member.email or member.website or member.twitter or member.linkedin or member.acl %}
	  <span class="invisible">A</span>
	  {% endunless %}
	  </div>
	</div>  
  </div> 
