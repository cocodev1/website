<div class="col-sm-6 my-2">
	<div class = "card text-center" style="height:100%"> 	  
	  {% if project.image %}
		{% if project.website %}
		  <a href="{{ project.website }}">
		{% else %}
		  <span>
		{% endif %}
		<img class="card-img-top pt-2 px-4" src="../{{ project.image }}" alt="Logo of {{ project.name }}"/>	  
		{% if project.website %}
		  </a>
		{% else %}
		  </span>
		{% endif %}         
	  {% endif %}	  		
	  <div class = "card-body pt-2">
		<h6 class = "card-title">
		  {% if project.website %}
			<a href="{{ project.website }}">
		  {% endif %}
		  {{ project.name }}
		  {% if project.website %}
			</a>
		  {% endif %}
		</h6>
		<p class="card-text text-muted">
		  {{ project.description }}
		</p>
	  </div>
	  <div class="card-footer">
		{% if project.agency %}
		  {{ project.agency }}
		{% endif %}
		<!-- Spacing below must not be changed otherwise parentheses look weird -->
		{% if project.start or project.end%}({% endif %}{% if project.start %}{{ project.start }}{% endif %}{% if project.start and project.end%} - {% endif %}{% if project.end %}{{ project.end }}{% endif %}{% if project.start or project.end%}){% endif %}
		{% unless project.agency or project.start or project.end %}
	      <span class="invisible">A</span>
	    {% endunless %}
	  </div>
	</div>
</div>
