<div class="col-sm-6 my-2">
	<div class = "card text-center" style="height:100%"> 	  
	  {% if resource.image %}
		{% if resource.website %}
		  <a href="{{ resource.website }}">
		{% else %}
		  <span>
		{% endif %}
		<img class="card-img-top pt-2 px-4" src="../{{ resource.image }}" alt="Logo of {{ resource.name }}"/>	  
		{% if resource.website %}
		  </a>
		{% else %}
		  </span>
		{% endif %}         
	  {% endif %}	  		
	  <div class = "card-body pt-2">
		<h6 class = "card-title">
		  {% if resource.website %}
			<a href="{{ resource.website }}">
		  {% endif %}
		  {{ resource.name }}
		  {% if resource.website %}
			</a>
		  {% endif %}
		</h6>
		<p class="card-text text-muted">
		  {{ resource.description }}
		</p>		
	  </div>	  
	</div>
</div>
