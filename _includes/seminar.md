<div class="accordion-item">
	<h2 class="accordion-header" id="panelSeminars-heading{{ pastorfuture }}{{ forloop.index }}">
	  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelSeminars-collapse{{ pastorfuture }}{{ forloop.index }}" aria-expanded="true" aria-controls="panelSeminars-collapse{{ pastorfuture }}{{ forloop.index }}">
		{{ seminar.date | date: "%B %d, %Y" %}}: {{ seminar.speaker }} - {{ seminar.title }}
	  </button>
	</h2>
	<div id="panelSeminars-collapse{{ pastorfuture }}{{ forloop.index }}" class="accordion-collapse collapse hide" aria-labelledby="panelSeminars-heading{{ pastorfuture }}{{ forloop.index }}">
	  <div class="accordion-body">
          <h5> {{ seminar.title }}</h5> <br/>
          <h6>
	    <span class="text-muted">
	    {% if seminar.website %}
	      <a href="{{ seminar.website }}">
	    {% endif %}
	    <b>{{ seminar.speaker }}</b>
	    {% if seminar.website %}
		  </a>
	    {% endif %} ({{ seminar.university }})
            
	    </span>
	    </h6>
	    
	    {% if seminar.abstract %}
		  <p><strong>Abstract:</strong> {{ seminar.abstract }}</p>
		{% endif %}
		<p>
		{% if seminar.time %}
		  <b>When:</b> {{ seminar.date | date: "%B %d, %Y" }} at {{ seminar.time }}
		{% endif %}
		{% if seminar.location %}
		  | <b>Where:</b> {{ seminar.location }}
		{% endif %}
		{% if seminar.language %}
		  | <b>Language:</b> {{ seminar.language }}
		{% endif %}
		{% if seminar.slides %}
		  | <b><a href="../{{ seminar.slides }}">Slides</a></b>
		{% endif %}
        {% if seminar.video %}
		  | <b><a href="{{ seminar.video }}" target="_blank">Watch video</a></b>
		{% endif %}
		</p>
	  </div>
	</div>
</div>
