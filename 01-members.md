---
layout: page
title: Members
permalink: /members/
---

<!-- To edit the list of members, you must modify _data/members.yml -->

<p><br></p>


<!--------------------------------------------------------------------->

{% assign currentsorted = site.data.members | where: "end", nil | where_exp: "member", "member.title contains 'professor' or member.title contains 'researcher'" | sort: "name" %}
<div class = "container-fluid">
  <div class = "row d-flex justify-content-center">  
  {% for member in currentsorted %}    
    {% include member.md %}     
  {% endfor %}
  </div>
</div>

<hr class="my-5"/>
<!--------------------------------------------------------------------->

## PhDs and Post-docs

{% assign currentsorted = site.data.members | where: "end", nil | where_exp: "member", "member.title contains 'PhD' or member.title contains 'Post-doc'  or member.title contains 'postdoc' or member.title contains 'engineer'" | sort: "name" %}
<div class = "container-fluid">
  <div class = "row d-flex justify-content-center">  
  {% for member in currentsorted %}    
    {% include member.md %}     
  {% endfor %}
  </div>
</div>

<hr class="my-5"/>
<!--------------------------------------------------------------------->




## Past members
<p><br></p>
{% assign currentsorted = site.data.members | where_exp: "member", "member.end" | sort: "start" | reverse %}


  <div class = "row">  
  {% for member in currentsorted %}    
    - {{ member.name }},
    {{ member.title }},
    {{ member.start }}
    <p><br></p>
  {% endfor %}
  </div>


